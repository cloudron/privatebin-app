#!/bin/bash

set -eu

mkdir -p /app/data/data /app/data/custom_template/css /app/data/custom_template/img /app/data/custom_template/js

echo "==> Creating config"
if [[ ! -f /app/data/conf.php ]]; then
    cp /app/code/cfg/conf.sample.php /app/data/conf.php

    # Enable file uploading
    sed -i "s#fileupload = false#fileupload = true#" /app/data/conf.php
fi

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

echo "==> Changing permissions"
chown -R www-data:www-data /app/data/

echo "==> Staring PrivateBin"

APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND

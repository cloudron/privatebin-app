FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# renovate: datasource=github-releases depName=PrivateBin/PrivateBin versioning=semver
ARG PRIVATEBIN_VERSION=1.7.6

# Install PrivateBin
RUN curl -Ls https://github.com/PrivateBin/PrivateBin/archive/${PRIVATEBIN_VERSION}.tar.gz | tar -xzf - --strip 1 -C /app/code \
    && mv .htaccess.disabled .htaccess \
    && rm *.md \
    && ln -s /app/data/custom_template/custom.php /app/code/tpl/custom.php \
    && ln -s /app/data/custom_template/css /app/code/css/custom \
    && ln -s /app/data/custom_template/js /app/code/js/custom \
    && ln -s /app/data/custom_template/img /app/code/img/custom \
    && ln -s /app/data/conf.php /app/code/cfg/conf.php \
    && ln -s /app/data/data /app/code/data \
    && chown -R www-data.www-data /app/code

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
RUN a2enmod rewrite
COPY apache/privatebin.conf /etc/apache2/sites-enabled/privatebin.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

# configure mod_php
RUN crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_filesize 256M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_size 256M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP post_max_size 256M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP memory_limit 256M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP max_execution_time 200 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_divisor 100

RUN cp /etc/php/8.1/apache2/php.ini /etc/php/8.1/cli/php.ini

RUN ln -s /app/data/php.ini /etc/php/8.1/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.1/cli/conf.d/99-cloudron.ini

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]

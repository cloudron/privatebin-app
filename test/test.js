#!/usr/bin/env node

/* jslint node:true */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const MESSAGE2 = 'Upload/Download Text File secret message';
    let pasteUrl;

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function clearCache() {
        await browser.manage().deleteAllCookies();
        await browser.quit();
        browser = null;
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        chromeOptions.addArguments(`--user-data-dir=${await fs.promises.mkdtemp('/tmp/test-')}`); // --profile-directory=Default
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    }

    async function checkUpload() {
        const testFilePath = `${path.resolve(__dirname, '.')}/test.txt`;

        await browser.get('https://' + app.fqdn);
        await browser.findElement(By.id('message')).sendKeys(MESSAGE2);
        await browser.findElement(By.id('file')).sendKeys(testFilePath);
        await browser.findElement(By.id('attach')).click();
        await browser.findElement(By.id('sendbutton')).click();
        await browser.sleep(5000);
        pasteUrl = await browser.findElement(By.id('pasteurl')).getText();
        console.log(`pasteUrl is ${pasteUrl}`);
        // pasteUrl is our unique secret URL
    }

    async function checkDownload() {
        await browser.get('about:blank'); // chrome doesn't reload if url is same
        await browser.sleep(2000);
        await browser.get(pasteUrl);
        await browser.sleep(2000);
        const text = await browser.findElement(By.id('prettyprint')).getText();
        if (text !== MESSAGE2) throw new Error(`Message mismatch: Actual: ${text} Expected: ${MESSAGE2}`);
        const text2 = await browser.findElement(By.id('attachment')).getText();
        if (!text2.includes('Download attachment')) throw new Error('No attachment');
        const href = await browser.findElement(By.xpath('//div[@id="attachment"]/a')).getAttribute('href');
        if (!href.includes(app.fqdn)) throw new Error('href does not include app fqdn');
        console.log(href);
        await browser.get(href); // Let's view our text file in the browser
        await browser.sleep(2000);
        const bodyContents = await browser.findElement(By.css('body')).getText();
        if (bodyContents !== 'Test txt file upload') throw new Error('File contents does not match');
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.startsWith(LOCATION); })[0];
        expect(app).to.be.an('object');
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can get the main page', async function () {
        const response = await fetch('https://' + app.fqdn);
        expect(response.status).to.eql(200);
    });

    it('can upload file', checkUpload);
    it('can download file', checkDownload);

    it('can restart app', function () { execSync('cloudron restart --app ' + app.id, EXEC_ARGS); });

    it('can download file', checkDownload);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can download file', checkDownload);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // test update
    it('clear cache', clearCache);
    it('can install previous version from appstore', function () { execSync(`cloudron install --appstore-id info.privatebin.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can upload file', checkUpload);

    it('clear cache', clearCache);
    it('can update', function () { execSync('cloudron update --app ' + LOCATION, EXEC_ARGS, EXEC_ARGS); });

    it('can download file', checkDownload);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
